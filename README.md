# Conway's Game of Life Ruby gem template

* Follow [these rules](http://c2.com/cgi/wiki?XpSimplicityRules). Apply what you
  learned in the class about SOLID, refactoring and patterns.
* Implement [this exercise](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules).
* Be exhaustive and thorough, question yourself all the time, to the point of not
  finishing the exercise.
